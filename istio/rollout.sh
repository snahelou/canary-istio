#!/bin/ash

healthcheck(){
	echo "Starting Heathcheck"
	h=true
	#Start custom healthcheck
	output=$(kubectl get pods -l app="$CANARY_HOST_NAME" -n $NAMESPACE --no-headers)
	s=$(echo "$output" | awk '{s+=$4}END{print s}')
	c=$(echo "$output" | wc -l)

	if [ "$s" -gt "2" ]; then
		h=false
	fi
	#End custom healthcheck
	if [ ! $h == true ]; then
		cancel
		echo "Exit failed"
	else
		echo "Service healthy."
	fi
}

cancel(){
	echo "Cancelling rollout"
	m="cancel"
	cp istio/canary.yml canary_$m.yml
	echo "  - route:" >> canary_$m.yml 
    echo "    - destination:" >> canary_$m.yml
    echo "        host:" "$CURRENT_HOST_NAME" >> canary_$m.yml
    echo "        port:" >> canary_$m.yml
    echo "          number:" 80 >> canary_$m.yml
    echo "      weight:" "100" >> canary_$m.yml
    echo "Done building config..."
    cat canary_$m.yml
    istioctl -c $WORKSPACE/kubeconfig replace -f canary_$m.yml -n $NAMESPACE
    echo "Canary removed from network."
    exit 1
}

incrementservice(){
	#Pass $1 = canary traffic increment
	m=$1
	echo "Creating canary_$m.yml ..."
	cp istio/canary.yml canary_$m.yml

	echo "  - route:" >> canary_$m.yml #Always prefix the route
	
	if [ "$m" -lt "100" ]; then #Add old version
		echo "    - destination:" >> canary_$m.yml
	    echo "        host:" "$CURRENT_HOST_NAME" >> canary_$m.yml
	    echo "        port:" >> canary_$m.yml
	    echo "          number:" 80 >> canary_$m.yml
	    echo "      weight:" $((100-$m)) >> canary_$m.yml
	fi
    echo "Add Canary" #Add new version
    echo "    - destination:" >> canary_$m.yml
    echo "        host:" "$CANARY_HOST_NAME" >> canary_$m.yml
    echo "        port:" >> canary_$m.yml
    echo "          number:" 80 >> canary_$m.yml
    echo "      weight:" $m >> canary_$m.yml
    echo "Done building config..."
    cat canary_$m.yml
    echo "Applying canary_$m.yml"
    echo "Running istioctl -c $WORKSPACE/kubeconfig replace -f canary_$m.yml -n $NAMESPACE"
    istioctl -c $WORKSPACE/kubeconfig replace -f canary_$m.yml -n $NAMESPACE 
    if [ "$?" != "0" ]; then
        cancel
    fi
    COUNTER=0
    until [ $COUNTER -ge 30 ]; do echo -n "."; sleep 1; let COUNTER+=1; done
    echo "Traffic mix updated to $m% for canary."
}

mainloop(){
	while [ $TRAFFIC_INCREMENT -lt 100 ]
	do
		p=$((p + $TRAFFIC_INCREMENT))
		if [ "$p" -gt "100" ]; then
			p=100
		fi
		incrementservice $p

		if [ "$p" == "100" ]; then
			echo "Done"
			exit 0
		fi
		sleep 50s
		healthcheck
	done
}

if [ "$1" != "" ] && [ "$2" != "" ] && [ "$3" != "" ] && [ "$4" != "" ]; then
	echo "Volume Set"
	NAMESPACE=$1
	CURRENT_HOST_NAME=$2
	CANARY_HOST_NAME=$3
	TRAFFIC_INCREMENT=$4
	KUBE=$5
else
	#echo instructions
	echo "USAGE\n rollout.sh [NAMESPACE] [CURRENT_HOST_NAME] [CANARY_HOST_NAME] [TRAFFIC_INCREMENT]"
	echo "\t [NAMESPACE] - This should be set with current namespace"
	echo "\t [CURRENT_HOST_NAME] - The name of the service currently receiving traffic from the Istio gateway"
	echo "\t [CANARY_HOST_NAME] - The name of the new service we're rolling out."
	echo "\t [TRAFFIC_INCREMENT] - Integer between 1-100 that will step increase traffic"
	exit 1;
fi

echo $BASH_VERSION
p=0
mainloop
