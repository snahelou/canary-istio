#!/usr/bin/env groovy

def dockerImage
def helmRelease = "my-release"
def kubectl = "kubectl  --kubeconfig=kubeconfig"
def helm = "KUBECONFIG=kubeconfig helm"
def prodVersion
def prodImage
def canaryVersion = "$helmRelease-${env.BUILD_NUMBER}"

node {
    stage('checkout') {
        checkout scm
    }

    stage ('Build Docker') {
      dockerImage = docker.build("demo/canary:${env.BUILD_NUMBER}")
    }

     stage('publish docker') {
       docker.withRegistry('https://508629320582.dkr.ecr.eu-west-1.amazonaws.com','registry') {
         dockerImage.push "${env.BUILD_NUMBER}"
         dockerImage.push 'latest'
       }
    }

    withCredentials([kubeconfigContent(credentialsId: 'admin-gcp', variable: 'KUBECONFIG_CONTENT')]) {
      docker.withRegistry('https://508629320582.dkr.ecr.eu-west-1.amazonaws.com','registry') {
        docker.image("508629320582.dkr.ecr.eu-west-1.amazonaws.com/demo/kubetool:2").inside {

          stage ('Check kubectl / helm / istio') {
            writeFile file: "kubeconfig", text: "$KUBECONFIG_CONTENT"
            sh "${kubectl} get nodes"
            sh "${helm} list ${helmRelease}"
          }
        
          stage ('Get current version') {
            prodVersion = sh (
              script: "${kubectl} get service -l status=prod  -o=jsonpath='{\$.items[0].metadata.name}'",
              returnStdout: true
            ).trim()
            prodImage = sh (
              script: "${kubectl} get service -l status=prod -o=jsonpath='{\$.items[0].metadata.labels.version}'",
              returnStdout: true
            ).trim()
            
            // Add check if null = initDeploy => prodImage: 1 

            echo "Your Prod version is: ${prodVersion} - ${prodImage}"
          }
          
          
          stage('Upgrade'){
            sh """${helm} upgrade --install ${helmRelease} deploy/ --namespace=default \
   				--set deployment[0].track=release \
   				--set deployment[0].image.repository="508629320582.dkr.ecr.eu-west-1.amazonaws.com/demo/canary" \
   				--set deployment[0].image.tag="${prodImage}" \
                --set deployment[0].image.version="${prodImage}" \
   				--set deployment[0].image.pullSecret=regcred \
   				--set deployment[1].track=canary \
   				--set deployment[1].image.repository="508629320582.dkr.ecr.eu-west-1.amazonaws.com/demo/canary" \
   				--set deployment[1].image.tag="${env.BUILD_NUMBER}" \
   				--set deployment[1].image.version="${env.BUILD_NUMBER}" \
   				--set deployment[1].image.pullSecret=regcred
            """
          }
        
          stage('Rollout'){
            sh "ash istio/rollout.sh default ${prodVersion} ${canaryVersion} 25"
          }

          stage('Release'){
            sh """${helm} upgrade --install ${helmRelease} deploy/ --namespace=default \
                --set deployment[0].track=canary \
                --set deployment[0].image.repository="508629320582.dkr.ecr.eu-west-1.amazonaws.com/demo/canary" \
                --set deployment[0].image.tag="${env.BUILD_NUMBER}" \
                --set deployment[0].image.version="${env.BUILD_NUMBER}" \
                --set deployment[0].image.pullSecret=regcred
            """
            sh "${kubectl} label svc ${canaryVersion} status=prod"
          }
        }
      }
    }
  
}
